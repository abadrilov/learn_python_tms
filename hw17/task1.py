import logging
from task_05 import *
from task_05 import *
from random import choice, randint
from typing import Union

class Car(MainCar, MixinSpoiler):
    def __init__(self, model, color) -> None:
        super().__init__(model, color)
        self._vol_tank = 60

    def inc_speed(self, value):
        self.current_speed = value
        return self.current_speed
        
def fuel_consumption(self):
    if self.speed > 0 and self.speed < 40:
        count_tank = self._vol_tank - 10
        return count_tank
    elif self.speed > 40 and self.speed < 60:
        count_tank = self._vol_tank - 8
        return count_tank
    elif self.speed > 60 and self.speed < 90:
        count_tank = self._vol_tank - 6
        return count_tank
    elif self.speed > 90 and self.speed < 120:
        count_tank = self._vol_tank - 5.5
        return count_tank
    elif self.speed > 120 and self.speed < 140:
        count_tank = self._vol_tank - 8
        return count_tank
    else:
        count_tank = self._vol_tank - 11
        return count_tank
    
class SpeedLimitError(Exception):
    def __init__(self) -> None:
        super().__init__("Your car increased the speeed more than limit")

class FuelFinishError(Exception):
    def __init__(self) -> None:
        super().__init__("Fuel is empty")

class StopDrive(Exception):
    def __init__(self) -> None:
        super().__init__("The car was stopped")

class WrongInstance(Exception):
    def __init__(self) -> None:
        super().__init__("WrongInstance")

def data_logging(logger, current_way, way, value):
    for i in range(0, way, 10):
        if current_way - value < i <= current_way:
            logger.info(f'The remaining distance  - {way - i}')

def sign_limits():
    sign: list = [40, 60, 90, 120]
    return choice(sign)

def speed_more_sign(logger, car_obj, random_sign):
    try:
        raise SpeedLimitError
    except SpeedLimitError as s:
        logger.info(f'{s} - Warning!!')
        speed = car_obj.inc_speed(random_sign)

def fuel_enough(logger, car_object):
    try:
        raise FuelFinishError
    except FuelFinishError as f:
        logger.info(f'{f} - Warning! Out of fuel!')
        car_object.change_speed(30)

def stop_drive(logger):
    try:
        raise StopDrive
    except StopDrive as s:
        logger.info(f"{s} - stop!")

def class_instance(car_object, class_name):
    if not isinstance(car_object, class_name):
        try:
            raise WrongInstance
        except WrongInstance as er:
            print(f'{er} - Enter right object!')
            return False
    else:
        return True

def logged():
    """Setting up the logging module."""
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    logger_handler = logging.FileHandler('logg.log')
    logger_handler.setLevel(logging.INFO)
    logger_formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    logger_handler.setFormatter(logger_formatter)
    logger.addHandler(logger_handler)
    logger.info('Start!')
    print(logger)
    return logger


def drive_car(car_object: Car, way: int):
    current_way: Union[int, float] = 0
    current_fuel: Union[int, float] = 0
    logger = logged()
    while True:
        if not class_instance(car_object, Car):
            break
        if not class_instance(way, int):
            break
        speed: Union[int, float] = car_object.get_speed
        random_speed_sign: int = sign_limits()
        if speed > random_speed_sign:
            speed_more_sign(logger, car_object, random_speed_sign)
        speed = car_object.spoiler(5)
        fuel = fuel_consumption(speed)
        possible_way = (car_object.get_fuel_tank - current_fuel) * 100 / fuel
        if possible_way < speed * 1:
            current_way = possible_way
            current_fuel = fuel * possible_way / 100
            fuel_enough(logger, car_object)
            current_fuel = 0
            data_logging(logger, current_way, way, possible_way)
            continue
        else:
            current_way = speed * 1
            current_fuel = fuel * speed * 1 / 100
            data_logging(logger, current_way, way, speed * 1)
        if current_way >= way:
            stop_drive(logger)
            break