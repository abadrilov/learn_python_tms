"""
Реализовать дата-класс HostSettings.
Должен иметь свойства ip_addr, port, hostname, username, password, auth_type
"""

from dataclasses import dataclass

@dataclass
class HostSettings:
    ip_addr: str = []
    port: int = 22
    hostname: str = None
    username: str = None
    password: str = None
    auth_type: str = "ssh keys"