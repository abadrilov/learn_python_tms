"""
Реализовать мета-класс фабрику, которая
в зависимости от атрибута shape_type = ['Rectange', 'Triangle', 'Circle']
возвращает нужный класс с свойством square (площадь фигуры)
"""

from math import pi

def triangle(self):
    pi = (self.first_side * self.second_side * self.thirtd_side) / 2
    sq = (pi * (pi - self.first_sid) * (pi - self.second_side) * (pi - self.thirtd_side)) ** 0.5

def circle(self):
    return pi * self.radius ** 2

def rectange(self):
    return self.lenght * self.width

class Meta(type):
    def __new__(cls, shape_type, parents, attr):
        if shape_type == "Triangle":
            attr["square"] = triangle
        elif shape_type == "Circle":
            attr["square"] = circle
        elif shape_type == "Rectange":
            attr["square"] = rectange
        return super().__new__(cls, shape_type, parents, attr)

def test_meta():
    rectangle = Meta('Rectangle', (), {})
    assert rectangle.__dict__['square'] == rectangle
    rectangle = rectangle()
    assert 'square' in rectangle.__dir__