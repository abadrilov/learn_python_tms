"""
Класс-контейнер хранит экзепмляры класса Shape.
Реализациями Shape могут быть 'Rectange', 'Triangle', 'Circle'
Shape должен предоставлять интерфейс square
Предусмотреть проверки
Контейнер должен иметь свойство square - возвращающее общую площадь всех фигур
"""

#from publisher import Publisher
import abc
from typing import Union

class ABCShape(abc.ABC):

    @abc.abstractmethod
    def square(self):
        pass

class Rectange(ABCShape):
    def __init__(self, lenght: Union[float, int], width: Union[float, int]) -> None:
        self.lenght = lenght
        self.width = width
    def square(self):
        summ = int(self.lenght * self.width)
        return summ

class Conteiner:
    def __init__(self) -> None:
        self.shape: list = []
        self.square: float = 0.0
    
    def rectange_to_list(self, lenght: Union[float, int], width: Union[float, int]) -> list:
        rect = Rectange(lenght, width)
        self.shape.append(rect)
        self.square += rect.square()

def container_test():
    container = Conteiner()
    container.rectange_to_list(12, 4)
    assert container.square == 12

