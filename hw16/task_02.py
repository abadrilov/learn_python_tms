"""
Ex.2
реализовать класс, который имеет атрибут журнала логирования,
куда попадают строки вида
<дата-время вызова>-<класс>-<вызываемый метод>

* реализовать декоратор, которым можно будет оборачивать тот метод,
который мы захотим залогировать
"""
import logging
import sys
import logging
from logging import Formatter
from functools import wraps
from datetime import datetime


def dec_log(func):
    @wraps(func)
    def wrapper(self, *args, **kw):
        logger = logging.getLogger(self.__class__.__name__)
        logger.setLevel(logging.INFO)
        handler = logging.StreamHandler(stream=sys.stdout)
        handler.setFormatter(Formatter(fmt='[%(asctime)s: %(name)s-%(message)s'))
        logger.addHandler(handler)
        lg = logger.info(f"{func.__name__}-{func(self, *args, **kw)}")
        return lg
    return wrapper

class AnimalSpeed:
    def __init__(self, speed: int=0) -> None:
        self._speed = speed
    
    @dec_log
    def get_animal_speed(self, value: int=10):
        self._speed += value
        return self._speed

# не понимаю, как провести тест