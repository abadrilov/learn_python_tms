"""
Ex.1
реализовать общий для всех классов
counter и метод, который его изменяет на 1
изменения должны быть видны всем дочерним классам
"""

class Counter:
    
    counter = 0
    
    @classmethod
    def inc_counter(cls, value=1) -> int:
        cls.counter += value
        return cls.counter

class Inh2(Counter):
    pass
class Inh3(Counter):
    pass

def t_counter():
    counter = Counter()
    inh2 = Inh2()
    inh3 = Inh3()
    counter.inc_counter()
    inh2.inc_counter()
    inh3.inc_counter()
    assert (Counter.inc_counter, Inh2.inc_counter, Inh3.inc_counter()) == (2, 2, 2)