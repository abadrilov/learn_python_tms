from sql_connection import connectDB


def DepartmentsCreateDB(connection):
    cursor = connection.cursor()

    create_table = """
        CREATE TABLE IF NOT EXISTS Departments(
            name_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            manager_id INTEGER,
            location_id INTEGER
        );
        """

    insert_data = """
        INSERT INTO Departments (name, manager_id, location_id)
        VALUES
            ("Development", 100, 1),
            ("Legal", 101, 1),
            ("IT infrastructure", 102, 2)
        """

    cursor.execute(create_table)
    cursor.execute(insert_data)
    cursor.execute("COMMIT;")
    cursor.close()


def EmployeesCreateDB(connection):
    cursor = connection.cursor()

    create_table = """
        CREATE TABLE IF NOT EXISTS Employees(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT,
            last_name TEXT,
            email TEXT,
            phone_number INTEGER,
            hire_date TEXT,
            job_id INTEGER,
            manager_id INTEGER,
            location_id INTEGER
        );
        """

    insert_data = """
        INSERT INTO Employees (first_name, last_name, email, phone_number,
                                hire_date, job_id, manager_id, location_id)
        VALUES
            ("Saveliy", "Orlov", "orlovsaveliy@gmail.com", 87654024687, 2010,
            210, 20, 30),
            ("Makeeva", "Anna", "makeevaanna@gmail.com", 87654024698, 2011,
            230, 20, 30),
            ("Vinogradova", "Polina", "vinogradovapolina@gmail.com",
            87654024610, 2018, 240, 20, 30),
            ("Korneev", "Anton", "korneevanton@gmail.com", 87654024625,
            2013, 250, 20, 30)
        """

    cursor.execute(create_table)
    cursor.execute(insert_data)
    cursor.execute("COMMIT;")
    cursor.close()


def JobsCreateDB(connection):
    cursor = connection.cursor()

    create_table = """
        CREATE TABLE IF NOT EXISTS Jobs(
            title_id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            min_salary INTEGER,
            max_salary INTEGER
        );
        """

    insert_data = """
        INSERT INTO Jobs (title, min_salary, max_salary)
        VALUES
            ("Programmer", 100000, 150000),
            ("Lawyer", 50000, 100000),
            ("Engineer", 120000, 140000)
        """

    cursor.execute(create_table)
    cursor.execute(insert_data)
    cursor.execute("COMMIT;")
    cursor.close()


def LocationCreateDB(connection):
    cursor = connection.cursor()

    create_table = """
        CREATE TABLE IF NOT EXISTS Location(
            stree_id INTEGER PRIMARY KEY AUTOINCREMENT,
            street_address TEXT,
            postal_code INTEGER,
            city TEXT,
            country_id INTEGER
        );
        """

    insert_data = """
        INSERT INTO Location (street_address, postal_code, city, country_id)
        VALUES
            ("Bulvar Rakasovskogo 26, 1", 658265, "Moscow", 77),
            ("Gerasimova 23, 56", 601120, "Vladimir", 33),
            ("Lenina 124, 76", 501122, "Tver", 65)
        """

    cursor.execute(create_table)
    cursor.execute(insert_data)
    cursor.execute("COMMIT;")
    cursor.close()


def CountriesreateDB(connection):
    cursor = connection.cursor()

    create_table = """
        CREATE TABLE IF NOT EXISTS Countries(
            Name_id INTEGER PRIMARY KEY AUTOINCREMENT,
            Name TEXT
        );
        """

    insert_data = """
        INSERT INTO Countries (Name)
        VALUES
            ("Russia")
        """

    cursor.execute(create_table)
    cursor.execute(insert_data)
    cursor.execute("COMMIT;")
    cursor.close()


lst_func = ["DepartmentsCreateDB", "EmployeesCreateDB", "JobsCreateDB",
            "LocationCreateDB", "CountriesreateDB"]

if __name__ == '__main__':
    connection = connectDB("./learn_python_tms/hw19/dev.db")
    DepartmentsCreateDB(connection)
    EmployeesCreateDB(connection)
    JobsCreateDB(connection)
    LocationCreateDB(connection)
    CountriesreateDB(connection)
