from sql_connection import connectDB


def get_workers_count(connection) -> int:
    cursor = connection.cursor()
    get_count = """SELECT title from jobs"""
    cursor.execute(get_count)
    r = cursor.fetchall()
    print(r)


connection = connectDB("./learn_python_tms/hw19/dev.db")
get_workers_count(connection)


def get_city(connection) -> int:
    cursor = connection.cursor()
    get_count = """SELECT city from Location ORDER BY city"""
    cursor.execute(get_count)
    r = cursor.fetchall()
    print(r)


connection = connectDB("./learn_python_tms/hw19/dev.db")
get_workers_count(connection)
get_city(connection)