"""
Написать функцию, которая определяет, является переданная строка палиндромом
Написать тесты, в тч параметризационные

"""
import pytest


def check_polindrom(string) -> str:
    revers_string = "".join(reversed(string))
    if revers_string == string:
        return True
    return False


string = "MALAYALAM"
print(check_polindrom(string))


@pytest.mark.parametrize("line, result", [("Dorn", False), ("radar", True), (4, None)])
def test_palindrome(line, result):
    assert check_polindrom(line) == result
