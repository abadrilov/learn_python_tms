create_author_table="""
CREATE TABLE IF NOT EXIST Author
(
    id          SERIAL PRIMARY KEY,
    Author_name TEXT
)
"""

create_book_table = """
CREATE TABLE IF NOT EXIST Book
(
    id          SERIAL PRIMARY KEY,
    Book_name   TEXT
    Author_id   INTEGER,
    published   DATE,
)
"""

create_publishe_table = """
CREATE TABLE IF NOT EXIST Publisher
(
    id             SERIAL PRIMARY KEY,
    Publisher_name TEXT,
    Address        TEXT
)
"""