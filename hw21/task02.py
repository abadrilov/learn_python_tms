"""
Написать фикстуру, которая окрывает соединение к тестовой базе данных и возвращает объект
сессии/соединения.
Из less_020 с помощью ORM записать в базу данные о книгах/издателях.
Написать тест, который проверяет, что все записи были добавлены в бд.
После выполнения теста соединение к бд должно быть закрыто в фикстуре

"""
import pytest
import sqlalchemy as db_conn

from sqlalchemy.orm import sessionmaker, Session
from models import Book, Author, Publisher, metadata

from contextlib import contextmanager
from typing import ContextManager


DB_CONNECTION = "postgresql://learn:learn@localhost:7432/learn"
engine = db_conn.create_engine(DB_CONNECTION).connect()
DBSession = sessionmaker(bind=engine)
metadata.create_all(engine)


@contextmanager
def connectDB(with_commit=False) -> ContextManager[Session]:
    session = DBSession()
    try:
        yield session
        if with_commit:
            session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


author = [
    ("Stiven_King"),
    ("Piter_Petegru"),
    ("Anton_Badrilov"),
]

publisher = [
    ("Rosman", "NY"),
    ("NewRoman", "LA")
    ("CinemaBook", "Moscow"),
]

books = [
    ("HarryPotter", "1999-03-07"),
    ("BillySimmers", "2022-05-09"),
    ("Your Saturday", "2005-02-29"),
]


def add_author(data):
    with connectDB(with_commit=True) as session:
        for name in data:
            author = Author(author_name=name)
            session.add(author)


def add_publisher(data):
    with connectDB(with_commit=True) as session:
        for name, contacts in data:
            publisher = Publisher(publisher_name=name, contact=contacts)
            session.add(publisher)


def add_book(data):
    with connectDB(with_commit=True) as session:
        for name, dates in data:
            book = Book(book_name=name, date_published=dates)
            session.add(book)


def test_inser_publisher(conectDB):
    results = connectDB.query(Publisher).all
    assert len(results) == len(publisher)
