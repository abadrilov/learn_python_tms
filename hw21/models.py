from sqlalchemy import Column, Date, ForeignKey, Integer, Text, SmallInteger, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import sqlalchemy as db_conn
from sqlalchemy.orm import sessionmaker, Session


Base = declarative_base()
metadata = Base.metadata


class Author(Base):
    __tablename__ = "author"
    
    author_id = Column(SmallInteger, primary_key=True)
    author_name = Column(String(25))


class Publisher(Base):
    __tablename__ = "publisher"

    publisher_id = Column(SmallInteger, primary_key=True)
    publisher_name = Column(String)
    contact = Column(String)


class Book(Base):
    __tablename__ = "book"

    book_id = Column(SmallInteger, primary_key=True)
    book_name = Column(String)
    date_published = Column(Date)
    author_id = Column(ForeignKey("author.author_id"))
    publisher_id = Column(ForeignKey("publisher.publisher_id"))

    author = relationship("Author")
    publisher = relationship("Publisher")
