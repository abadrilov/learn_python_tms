"""
Написать фикстуру, которая генерирует строку случайных символов заданной длины.

"""
import pytest
import string
import random
from typing import Union

@pytest.fixture
def generator_str():
    def string_generator(lenght: int):
        result: Union[str, int] = "".join((random.choice(string.ascii_lowercase + string.digits) for _ in range(lenght)))
        return result
    return string_generator
