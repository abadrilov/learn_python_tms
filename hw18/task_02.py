"""
Ex2
Реализовать вычисление факториала через генератор

"""
import math


def factorial_gen(count):
    fact = (n for n in range(1, count + 1))
    value: int = 1
    while True:
        try:
            next_val = next(fact)
            value *= next_val
        except StopIteration:
            break


def test_fac():
    gen_fac = factorial_gen(4)

    assert next(gen_fac) == 1
    assert next(gen_fac) == 2
    assert next(gen_fac) == 6
    assert next(gen_fac) == 28
    assert next(gen_fac) == math.factorial(4)
