"""
Ex.3
Написать функцию-генератор, которая сканирует папки/файлы и возвращает полный путь к файлам, размер которых больше 1 МБ
'/ful_path_to_file', 'file_size'
Результат записывается в файл out.txt


"""
import os
import logging

logging.basicConfig(filename="log.txt",
                            filemode="a",
                            format="%(asctime)s %(levelname)s %(message)s",
                            datefmt="%H:%M:%S",
                            level=logging.INFO)

logger = logging.getLogger()


def file_scanner(path_file: str = "./hw15", filesize: int = 10):
    for dirpath, dirnames, filenames in os.walk(path_file):
        for file_name in filenames:
            if os.path.getsize(os.path.join(dirpath, file_name)) > filesize:
                logger.info(f"{os.path.getsize(os.path.join(dirpath, file_name))}")
