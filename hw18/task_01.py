"""
Ex1
Класс NamesIterable принимает коллекцию Фамилия Имя Отчество.
Реализовать итератор, который будет возвращать элементы, ориентируясь на Имя
Разделители могут быть ' ', '_', '-'

names = ['Ivanov Ivan Ivanovich', 'Petrova Olga Sergeevna', 'Petrichenko_Olga_Vladimirovna']

iterable = NamesIterable(names, 'Olga')
iterator = iter(iterable)
assert list(iterator) == ['Petrova Olga Sergeevna', 'Petrichenko_Olga_Vladimirovna']

"""


class NamesIterable(object):
    def __init__(self, full_name, first_name) -> None:
        self._full_name: list = full_name
        self._first_name: str = first_name

    # @property
    # def get_names(self):
    #     return self._full_name

    def __iter__(self):
        self._count_iter = 1
        return self

    def __next__(self):
        if self._first_name in self._full_name:
            return self._full_name
        else:
            raise StopIteration


names = ["Ivanov Ivan Ivanovich", "Petrova Olga Sergeevna", "Petrichenko_Olga_Vladimirovna"]

fname = NamesIterable(names, "Ivan")
myiter = iter(fname)

for i in myiter:
    print(i)