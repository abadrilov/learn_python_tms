from db_connection_to_pg import connectionDB
from db_models import Product, Category, Supplier
from sqlalchemy.sql import func


def get_product():
    with connectionDB() as connection:
        sum_of_product = func.sum(Product.units_in_stock)
        query = connection.query(Category.category_name, sum_of_product).join(Category)
        group_by_category = query.group_by(Category.category_name)
        results = group_by_category.order_by(sum_of_product.desc()).all()
        
        
        for result in results:
            print(result)


get_product()