from db_connection_to_pg import connectionDB
from db_models import Order, Customer


def get_customets_that_has_not_order():
    with connectionDB() as connection:
        subquery = connection.query(Order.customer_id).subquery()
        results = connection.query(Customer).filter(
            Customer.customer_id.notin_(subquery)).with_entities(
                            Customer.customer_id, Customer.company_name)

        for result in results:
            print(result)


get_customets_that_has_not_order()