import sqlalchemy as db_conn
from db_settings import DB_CONNECTION
from sqlalchemy.orm import sessionmaker, Session

from contextlib import contextmanager
from typing import ContextManager


engine = db_conn.create_engine(DB_CONNECTION)
DBSession = sessionmaker(bind=engine)


@contextmanager
def connectionDB(with_commit=False) -> ContextManager[Session]:
    session = DBSession()
    try:
        yield session
        if with_commit:
            session.commit()
    except:
        session.rollback()
        raise
    session.close()
