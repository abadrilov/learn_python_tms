from db_connection_to_pg import connectionDB
from db_models import Product, Category, Supplier


def get_product():
    with connectionDB() as connection:
        results = connection.query(Product).join(Category).filter(Product.discontinued == 1, Category.category_name.in_((
                                                            "Condiments", "Meat/Poultry")), Product.units_in_stock <= 100
                                                            ).with_entities(Product.product_name, Product.units_in_stock, 
                                                            Supplier.contact_name, Supplier.phone )


        for result in results:
            print(result)


get_product()