from db_connection_to_pg import connectionDB
from db_models import Order, Employee, Shipper, Customer


def get_customets_and_employees():
    with connectionDB() as connection:
        results = connection.query(Order).join(Employee).join(
            Shipper).join(Customer).filter(Customer.city == "London",
                                        Employee.city == "London",
                                        Shipper.company_name == "Speedy Express").with_entities(
                                                        Customer.company_name,
                                                        Employee.last_name,
                                                        Employee.first_name)

        for result in results:
            print(result)


get_customets_and_employees()