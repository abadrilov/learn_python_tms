"""
Loops
Написать цикл, который опрашивает пользователя и
выводит рандомное число.
Цикл должен прерываться по символу Q(q)

"""
from random import randint

def random_count():
    while True:
        input_user = str(input("Input some words: "))
        print(randint(1, 10000))
        if input_user.lower() == "q":
            break
random_count()
