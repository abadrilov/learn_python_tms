"""
Ex.2
Задача
Найти сумму всех чисел, меньших 1000, кратных 3 и 7
Реализовать через filter/map/reduce

"""
from functools import reduce

# varable
nmbr = list(range(1, 100))

# filter()
def count_filter(number):
    if number % 3 == 0 and number % 7 == 0:
        return True

filter_func = list(filter(count_filter, nmbr))
print(sum(filter_func))

# map()
map_func = list(map(lambda x: x if x % 3 == 0 and x % 7 == 0 else False, nmbr))
print(sum(map_func))

reduce()
def summ_all_count_reduce(x, y):
    if (x % 3 == 0) and (y % 7 == 0):
        return x, y

reduce_func = reduce(summ_all_count_reduce, nmbr)
print(reduce_func)
