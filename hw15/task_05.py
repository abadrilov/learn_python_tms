"""

Ex.5
Реализовать класс Car в соответствии с интерфейсом.
В нем реализовать метод speed, возвращающий текущую скорость.
Реализовать декоратор метода - в случае превышения скорости - декоратор
должен логировать в logger.error сообщение о превышении лимита

"""
import logging
from task_06 import *
from task_04 import *

class MainCar(MixinSpoiler, ABCCar):
    def __init__(self, model, color) -> None:
        self.model = model
        self.color = color
        self.list_status_doors = []
        self.speed = 0

    def close_car(self) -> list:
        question_close = str(input("do you want to close the car?: "))
        if question_close == "yes":
            return self.list_status_doors.append("1")

    def open_car(self) -> int:
        question_open = str(input("do you want to close the car?: "))
        if question_open == "yes":
            return self.list_status_doors.append("0")
    
    def check_door(self) -> list:
        if "0"[::-1] in self.list_status_doors:
            return "The car is opened"
        else:
            return "The car is closed"

    def speed_limit(func):
        limit_speed = 100
        def wrapper(*args):
            if func(*args) > limit_speed:
                logging.ERROR("This speed was increased")
            results = func(*args)    
            return results
        return wrapper

    @speed_limit
    def current_speed(self, value):
        if self.speed == 0:
            self.speed += value
        return self.speed

car1 = MainCar("red", "bmw", "x5")
print(car1.current_speed(90))