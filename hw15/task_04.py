"""
Реализовать интерфейс класса Car.

"""
import abc

class ABCCar(abc.ABC):
    @abc.abstractmethod
    def close_car(self):
        raise NotImplementedError
    
    @abc.abstractmethod
    def open_car(self):
        raise NotImplementedError

    @abc.abstractmethod
    def check_door(self):
        raise NotImplementedError
