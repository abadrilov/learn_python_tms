"""
Ex.6
Реализовать класс-миксин, добавляющий классу Car атрибут
spoiler.
Spoiler должен влиять на Car.speed , увеличивая ее на значение N.

"""
class MixinSpoiler:
    def spoiler(self, value=10):
        self.speed += value
        return self.speed
