"""
Реализовать декоратор кэширования вызова функции
В случае, если вызывается функция c одинаковыми
параметрами, то результат не должен заново вычисляться,
а возвращаться из хранилища

"""
import time

def cache(func) -> dict:
    CACHE: dict = {}
    def wrapper(*args):
        if args in CACHE:
            return CACHE[args]
        res = func(*args)
        CACHE[args] = res
        return res
    return wrapper

@cache
def sum(a, b):
    return a + b

print(sum(100, 100))
print(sum(100, 100))
print(sum(100, 100))